'use strict';
const fetch = require('node-fetch');
const _ = require('lodash');
const es = require('elasticsearch')
const userMapping = require('./users_types_mapping.json')
const contentsMapping = require('./contents_types_mapping.json')
const awses = require('http-aws-es')
const AWS = require('aws-sdk');

const TRAENA_API_BASE_URL = process.env.TRAENA_API_BASE_URL; //'http://localhost:3001';
const ELASTIC_SEARCH_API_BASE_URL = process.env.ELASTIC_SEARCH_API_BASE_URL; //'https://search-traena-dev-y7zy5n7xdjp7gkvmzmuliqgzhu.us-west-2.es.amazonaws.com';

const awsCredentials = AWS.config.credentials;

const client = es.Client({
  hosts: ELASTIC_SEARCH_API_BASE_URL,
  connectionClass: awses,
  amazonES: {
    region: 'us-west-2',
    credentials: awsCredentials
  },
  log: 'trace'
});

exports.handler = (event, context, callback) => {
  const orgId = event.organizationId;

  // Guards
  if (!orgId || !TRAENA_API_BASE_URL || !ELASTIC_SEARCH_API_BASE_URL) {
    console.log(`
        orgId: ${orgId}
        TRAENA_API_BASE_URL: ${TRAENA_API_BASE_URL}
        ELASTIC_SEARCH_API_BASE_URL: ${ELASTIC_SEARCH_API_BASE_URL}
      `);
    callback('Missing an environment variable!');
    return;
  }

  // CONTENT FETCH
  // TODO - Implement paging
  console.log(`Fetching content for orgId: ${orgId}`);
  const contentsUrl = `${TRAENA_API_BASE_URL}/api/v1/gather/organizations/${orgId}/contents`;
  const contentsPromise =
    fetch(contentsUrl)
    .then(response => response.json())
    .then(contents => deleteIndex('contents', contents))
    .then(contents => createIndexWithMapping('contents', contentsMapping, contents))
    .then(contents => createAlias('contents', orgId, contents))
    .then(contents => Promise.all(contents.map(item => indexDocument('contents', _.snakeCase(item.body.type), item, orgId))));

  // USER FETCH
  // TODO - Maybe implement paging
  console.log(`Fetching users for orgId: ${orgId}`);
  const usersUrl = `${TRAENA_API_BASE_URL}/api/v1/gather/organizations/${orgId}/users`;
  const usersPromise =
    fetch(usersUrl)
    .then(response => response.json())
    .then(users => deleteIndex('users', users))
    .then(users => createIndexWithMapping('users', userMapping, users))
    .then(users => createAlias('users', orgId, users))
    .then(users => Promise.all(users.map(item => indexDocument('users', 'profile', item, orgId))));

  Promise.all([contentsPromise, usersPromise])
    .then(() => {
      console.log(`**Done pushing content to elasticsearch for orgId: ${orgId}`);
      callback(null, 'success!');
    })
    .catch((err) => {
      console.log(err);
      callback(err);
    });
}

const createAlias = (index, orgId, values) => {
  const aliasName = `${index}_org_${orgId}`;

  console.log(`Creating index alias: ${index} -> ${aliasName}`)
  return client.indices.putAlias({
      index: index,
      name: aliasName,
      body: {
        filter: {
          term: {
            organizationId: orgId
          }
        }
      }
  })
  .then((body) => {
    console.log(`Success created index alias: ${index} -> ${aliasName}`)
    return values;
  })
  .catch((error) => {
    console.log(`Error: ${error}`);
    throw Error(`Could not add alias for index ${index}`);
  });
}

const createIndexWithMapping = (index, mappingJson, values) => {
  console.log(`Creating index with mappings: ${index}`);

  return client.indices.create({
      index: index,
      body: mappingJson
    })
    .then((body) => {
      console.log(`Success: created index ${index}`);
      return values;
    })
    .catch((error) => {
      console.log(`Error: ${error}`);
      throw Error(`Could not add mapping to index ${index}`);
    });
}

const indexDocument = (index, type, body, orgId) => {
  console.log(`Indexing document at ${index}/${type}/${body.id}`);
  //Ensure doc contains the organizationId for filtering
  body.organizationId = orgId;

  return client.index({
      index: index,
      body: body,
      type: type,
      id: body.id
    })
    .then((responseBody) => {
      console.log(`Success: indexed ${type} ${body.id}`);
    })
    .catch((error) => {
      console.log(`Error: ${error}`);
      throw Error(`Could not index ${type} ${body.id}`);
    });
}

const deleteIndex = (index, values) => {
  console.log(`Deleting index ${index}`);
  return client.indices.delete({
      index: index
    })
    .then((responseBody) => {
      return values;
    })
    .catch((error) => {
      if(error.statusCode == 404) {
        console.log(`Deletion skipped as Index ${index} does not exist`)
        return values;
      } else {
        console.log(`Delete Index Error: ${error.message}`)
        throw Error(`Problem deleting index for orgId: ${orgId}`);
      }


    });
}
