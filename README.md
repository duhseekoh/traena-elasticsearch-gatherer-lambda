### ABANDONED - Look at https://github.com/Traena/elasticsearch-indexer instead

## Overview
*Purpose:* Queries data from Traena API, pushes into elasticsearch.

Currently queries for content and users for a specific traena organization. It makes a call to a set of /gather endpoints in the api, loops over the results and pushes them out to elasticsearch.

## Lambda Event
The organization is specified by id in the event that triggers the lambda.
```
{ "organizationId": 1 } // Whatever the id of the org is in the traena db
```

This is setup to run on an hourly basis using a cloudwatch scheduled event. (https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#rules:name=submit-organization-index-job)

## Elastic search
```
elasticsearch index = organization id
elasticsearch types = content, user
```

By having a separate index for each organization, we can gather content and users that a particular organization contains or has access to, and silo them.

## Environments
Dev: https://us-west-2.console.aws.amazon.com/es/home?region=us-west-2#traena-dev:dashboard

Staging: Not yet

Prod: Not yet
